#!/bin/sh

COPY_SERVICE=${COPY_SERVICE:-0}
KRB5_REALM=${KRB5_REALM:-EXAMPLE.ORG}
KRB5_KDC=${KRB5_KDC:-example_kdc_ip}
[ -z "$LDAP_AUTHZ_REGEXP" ] && \
    LDAP_AUTHZ_REGEXP="uid=(.*),cn=$LDAP_DOMAIN,cn=gssapi,cn=auth \
                       ldap:///{{ LDAP_BASE_DN }}??sub?(uid=\$1)"

export COPY_SERVICE KRB5_REALM KRB5_KDC LDAP_AUTHZ_REGEXP

[ -n "$LDAP_ADMIN_PASSWORD_FILE" ] && \
    export LDAP_ADMIN_PASSWORD=`cat $LDAP_ADMIN_PASSWORD_FILE`

[ -n "$LDAP_CONFIG_PASSWORD_FILE" ] && \
    export LDAP_CONFIG_PASSWORD=`cat $LDAP_CONFIG_PASSWORD_FILE`

/container/config.sh

chgrp openldap /var/run/saslauthd

[ "$#" -ne 0 ] && exec "$@"

[ "$COPY_SERVICE" = "1" ] && \
    exec /container/tool/run --copy-service

exec /container/tool/run
