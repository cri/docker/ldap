FROM osixia/openldap

RUN apt-get update \
    && apt-get install -y --no-install-recommends sasl2-bin ca-certificates \
    && apt-get clean

COPY ./config.sh ./container/config.sh
COPY ./docker-entrypoint.sh /container/docker-entrypoint.sh
COPY ./index.ldif /container/service/slapd/assets/config/bootstrap/ldif/custom/01-index.ldif
COPY ./access.ldif /container/service/slapd/assets/config/bootstrap/ldif/02-security.ldif
COPY ./slapd.conf /usr/lib/sasl2/slapd.conf

COPY ./kerberos_ldif.sh /container/kerberos_ldif.sh
RUN  /container/kerberos_ldif.sh

ENTRYPOINT ["/container/docker-entrypoint.sh"]
