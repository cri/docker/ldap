#!/bin/sh

ldif_folder=/container/service/slapd/assets/config/bootstrap/ldif/custom

sasl_ldif=$ldif_folder/02-sasl.ldif
authz_regexp_ldif=$ldif_folder/03-authzRegexp.ldif

echo "BASE $LDAP_BASE_DN" >> /etc/ldap/ldap.conf
echo "SASL_MECH GSSAPI" >> /etc/ldap/ldap.conf

if [ ! -f "$sasl_ldif" ]; then
    cat > $sasl_ldif << EOF
dn: cn=config
changetype: modify
replace: olcSaslRealm
olcSaslRealm: $KRB5_REALM
-
replace: olcSaslSecProps
olcSaslSecProps: none
EOF
fi

if [ ! -z "$LDAP_AUTHZ_REGEXP" ] && [ ! -f "$authz_regexp_ldif" ]; then
    cat > $authz_regexp_ldif  << EOF
dn: cn=config
changetype: modify
add: olcAuthzRegexp
EOF
    i=0
    oldIFS=$IFS
    IFS='|'
    for entry in $LDAP_AUTHZ_REGEXP; do
        echo "olcAuthzRegexp: {$i}"$entry >> $authz_regexp_ldif
        i=$((i + 1))
    done
    IFS=$oldIFS
fi

cat > /etc/krb5.conf << EOF
[libdefaults]
	default_realm = $KRB5_REALM
	ignore_acceptor_hostname = true
	rdns = false
EOF

if [ -n "$KRB5_KDC" ]; then
	cat >> /etc/krb5.conf << EOF
[realms]
	$KRB5_REALM = {
		kdc = $KRB5_KDC
	}
EOF

fi

cat >> /etc/krb5.conf << EOF
[domain_realm]
	$LDAP_DOMAIN = $KRB5_REALM
	.$LDAP_DOMAIN = $KRB5_REALM
EOF
